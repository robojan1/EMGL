#include <emgl/emgl_ui.h>

namespace emgl
{

	Element::Element(Element *parent, Point pos, Size size, int id) :
		_pos(pos), _size(size), _parent(parent), _id(id), 
		_onClickCallbackFunc(NULL), _userValue(NULL)
	{
		if (parent) {
			parent->AddChild(this);
		}
	}

	Element::~Element()
	{
		removeChildren();
	}

	void Element::setOnClickCallback(bool (*onClickCallbackFunc)(Element *, void *,bool), void *userValue)
	{
		_onClickCallbackFunc = onClickCallbackFunc;
		_userValue = userValue;
	}

	Size Element::GetSize() const
	{
		return _size;
	}

	Point Element::GetPos() const
	{
		return _pos;
	}

	Point Element::GetScreenPos() const
	{
		if (_parent) {
			return _parent->GetScreenPos() + GetPos();
		}
		else {
			return GetPos();
		}
	}

	Rectangle Element::GetRect() const
	{
		return Rectangle(_pos, _size);
	}

	Rectangle Element::GetClientRect() const
	{
		return Rectangle(Point(0, 0), _size);
	}

	int Element::GetID() const
	{
		return _id;
	}

	void Element::SetSize(Size size)
	{
		_size = size;
	}

	void Element::SetPos(Point pos)
	{
		_pos = pos;
		if (_parent) {
			_parent->Refresh();
		}
		else {
			Refresh();
		}
	}

	void Element::Refresh(PaintContext &PC, Rectangle &area)
	{
		this->Draw(PC, area);
		Size selfSize = GetSize();
		for (std::list<struct childinfo>::iterator child = _children.begin();
			child != _children.end(); child++)
		{
			Point pos = child->child->GetPos();
			PC.offsetOrigin(pos);
			Size size = child->child->GetSize();
			if (pos.x + size.width > selfSize.width)  size.width = selfSize.width - pos.x;
			if (pos.y + size.height > selfSize.height)  size.height = selfSize.height - pos.y;
			PC.setSize(size);
			Rectangle refreshArea = area - pos;
			child->child->Refresh(PC, refreshArea);
			PC.offsetOrigin(-pos);
		}
	}

	void Element::Refresh()
	{
		Point pos = GetPos();
		PaintContext PC(GetScreenPos(), GetSize());
		Rectangle area = GetRect() - pos;
		Refresh(PC, area);
	}

	void Element::Draw(PaintContext &PC, Rectangle &area)
	{

	}
	
	void Element::setParent(Element *parent)
	{
		if(_parent != NULL) {
			_parent->RemoveChild(this);
		}
		_parent = parent;
	}

	void Element::RemoveChild(Element *child) 
	{
		for (std::list<struct childinfo>::iterator it = _children.begin();
			it != _children.end(); it++) {
			if(it->child == child) {
				if(it->own) {
					it->child->removeChildren();
					delete it->child;
				}
				_children.erase(it);
				break;
			}
		}		
	}

	void Element::AddChild(Element *child, bool own)
	{
		struct childinfo info;
		info.child = child;
		info.own = own;
		child->setParent(this);
		_children.push_back(info);
	}

	

	void Element::removeChildren()
	{
		for (std::list<struct childinfo>::iterator child = _children.begin();
			child != _children.end(); child++) {
			if(!child->own) continue;
			child->child->removeChildren();
			delete child->child;
		}
		_children.clear();
	}

	bool Element::ProcessPressedEvent(Point pos, bool pressed)
	{
		if (OnPressedEvent(pos, pressed))
			return true;

		for (std::list<struct childinfo>::iterator child = _children.begin();
			child != _children.end(); child++) {
			if (child->child->ProcessPressedEvent(pos - child->child->GetPos(), pressed))
				return true;
		}

		return false;
	}

	bool Element::OnPressedEvent(Point pos, bool pressed)
	{
		if(_onClickCallbackFunc) {
			_onClickCallbackFunc(this, _userValue, pressed);
		}
		return false;
	}
}

